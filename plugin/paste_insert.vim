"
" This small plugin provides a simple "one shot paste" method, with a command
" or mapping to prefix opening an insert, with the 'paste' option
" automatically set after the insert ends, to avoid the annoyances caused by
" forgetting to do so.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_paste_insert') || &compatible
  finish
endif
let loaded_paste_insert = 1

" Command interface
command! -bar PasteInsert
      \ call paste_insert#()

" Normal mode mapping interface
nnoremap <Plug>PasteInsert
      \ :<C-U>PasteInsert<CR>
