" Autoloaded entry point function
function! paste_insert#() abort

  " Stop and do nothing if already in the middle of a paste
  if exists('#paste_insert#User#Start')
    return
  endif

  " Set up an event table
  augroup paste_insert
    autocmd!

    " Set up the paste
    autocmd User Start
          \ call s:Start()

    " On insert mode start, add leaving hook to complete operation
    autocmd InsertEnter *
          \ autocmd paste_insert InsertLeave *
                \ doautocmd paste_insert User Complete

    " Text changed outside insert mode, complete operation
    if exists('##TextChanged')
      autocmd TextChanged *
            \ doautocmd paste_insert User Complete
    else
      autocmd CursorMoved *
            \ if changenr() > b:paste_insert_changenr
            \|  doautocmd paste_insert User Complete
            \|endif
    endif

    " User waits too long in normal mode, timeout
    autocmd CursorHold *
          \ doautocmd paste_insert User Timeout

    " User leaves the buffer or window, abort
    autocmd BufLeave,WinLeave *
          \ doautocmd paste_insert User Abort

    " End the paste and clear the events table
    autocmd User Abort,Cancel,Complete,Timeout
          \ call s:Stop() | autocmd! paste_insert

  augroup END

  " Trigger the starting actions
  doautocmd paste_insert User Start

endfunction

" Keys that cancel a pending paste in normal mode
let s:cancel_keys = get(g:, 'paste_insert_cancel_keys', ['<C-C>', '<Esc>'])

" Start the paste: remap any cancel keys, set 'paste'
function! s:Start() abort
  if !exists('##TextChanged')
    let b:paste_insert_changenr = changenr()
  endif
  for key in s:cancel_keys
    if maparg(key, 'n') ==# ''
      execute join(['nnoremap', key,
            \ ':<C-U>doautocmd paste_insert User Cancel<CR>'])
    else
      echoerr key.' already mapped in normal mode, refusing to remap'
    endif
  endfor
  set paste paste?
endfunction

" Stop the paste: unset 'paste', restore prior function of cancel key
function! s:Stop() abort
  if !exists('##TextChanged')
    unlet b:paste_insert_changenr
  endif
  set nopaste paste?
  for key in s:cancel_keys
    execute join(['nunmap', key])
  endfor
endfunction
