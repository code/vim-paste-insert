paste\_insert.vim
=================

This small plugin provides a simple "one shot paste" method, with a command or
mapping to prefix opening an insert, with the `'paste'` option automatically
set after the insert ends, to avoid the annoyances caused by forgetting to do
so.

It includes a timeout if insert mode isn't entered within `'updatetime'`
seconds, or if the user navigates away from the buffer or window.  It can also
be cancelled with CTRL-C in normal mode.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
